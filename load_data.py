import json

if __name__=='__main__':
    data = []
    file_names = ['readings_15.txt','readings_20.txt','readings_48.txt','readings_59_5.txt']
    laser_distance = [15,20,48,59.5]
    for file_name in file_names:
    	with open(file_name) as f:
	    for line in f:
		data.append(json.loads(line))
    print("Length of data",len(data))
